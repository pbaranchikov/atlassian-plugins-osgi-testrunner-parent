package com.atlassian.plugins.osgi.test.rest;

import java.util.Collections;
import java.util.List;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since version
 */
public class TestDetailRepresentation
{
    @JsonProperty private final String classname;
    @JsonProperty private final String divId;
    @JsonProperty private List<String> allTestMethods;
    
    @JsonCreator
    public TestDetailRepresentation(@JsonProperty("classname") String classname)
    {
        this.classname = classname;
        this.divId = classname.replaceAll("\\.","-");
        this.allTestMethods = Collections.<String> emptyList();
    }

    public String getClassname()
    {
        return classname;
    }

    public String getDivId()
    {
        return divId;
    }

    public List<String> getAllTestMethods()
    {
        return allTestMethods;
    }

    public void setAllTestMethods(List<String> allTestMethods)
    {
        this.allTestMethods = allTestMethods;
    }
}

package com.atlassian.plugins.osgi.test.util;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @since version
 */
public class TestClassUtils
{
    public static enum TestType {Unit, Wired, IT}
    
    public static TestType getTestType(Class<?> testClass)
    {
        if(testClass.isAnnotationPresent(RunWith.class) && testClass.getAnnotation(RunWith.class).value().getName().equals(AtlassianPluginsTestRunner.class.getName()))
        {
            return TestType.Wired;
        }

        String[] packageParts = testClass.getPackage().getName().split("\\.");

        if(null != packageParts && packageParts.length > 0 && packageParts[0].equals("it"))
        {
            return TestType.IT;
        }
        
        return TestType.Unit;
    }
    
    public static List<String> getNonIgnoredMethodNames(Class<?> testClass)
    {
        ArrayList<String> names = new ArrayList<String>();
        
        if(testClass.isAnnotationPresent(Ignore.class))
        {
            return names;    
        }
        
        Method[] methods = testClass.getMethods();
        for(Method m : methods)
        {
            if((m.isAnnotationPresent(Test.class) && !m.isAnnotationPresent(Ignore.class)) || (m.getName().startsWith("test") && Modifier.isPublic(m.getModifiers())))
            {
                names.add(m.getName());
            }
        }
        
        return names;
    }

    public static List<String> getAllTestMethodNames(Class<?> testClass)
    {
        ArrayList<String> names = new ArrayList<String>();
        Method[] methods = testClass.getMethods();
        for(Method m : methods)
        {
            if(m.isAnnotationPresent(Test.class)|| (m.getName().startsWith("test") && Modifier.isPublic(m.getModifiers())))
            {
                names.add(m.getName());
            }
        }

        return names;
    }

    public static List<String> getIgnoredMethodNames(Class<?> testClass)
    {
        ArrayList<String> names = new ArrayList<String>();

        Method[] methods = testClass.getMethods();
        for(Method m : methods)
        {
            if(m.isAnnotationPresent(Test.class) && (m.isAnnotationPresent(Ignore.class) || testClass.isAnnotationPresent(Ignore.class)))
            {
                names.add(m.getName());
            }
        }

        return names;
    }
}

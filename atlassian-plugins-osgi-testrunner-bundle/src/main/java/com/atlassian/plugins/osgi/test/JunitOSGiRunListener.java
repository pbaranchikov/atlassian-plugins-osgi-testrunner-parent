package com.atlassian.plugins.osgi.test;

import java.util.HashSet;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.junit.runner.notification.RunNotifier;

/**
 * @since version
 */
public class JunitOSGiRunListener extends RunListener
{
    private final RunNotifier notifier;
    private Set<String> passedMethodNames;
    private Set<String> failedMethodNames;
    private Set<String> ignoredMethodNames;

    public JunitOSGiRunListener(RunNotifier notifier)
    {
        this.notifier = notifier;
        this.passedMethodNames = new HashSet<String>();
        this.failedMethodNames = new HashSet<String>();
        this.ignoredMethodNames = new HashSet<String>();
    }

    @Override
    public void testFailure(Failure failure) throws Exception
    {
        failedMethodNames.add(failure.getDescription().getMethodName());
    }

    @Override
    public void testFinished(Description description) throws Exception
    {
        String methodName = description.getMethodName();
        if(!failedMethodNames.contains(methodName) && !ignoredMethodNames.contains(methodName))
        {
            passedMethodNames.add(methodName);
        }
    }

    @Override
    public void testIgnored(Description description) throws Exception
    {
        ignoredMethodNames.add(description.getMethodName());
    }

    @Override
    public void testAssumptionFailure(Failure failure)
    {
        notifier.fireTestIgnored(failure.getDescription());
    }

    public Set<String> getPassedMethodNames()
    {
        return ImmutableSet.copyOf(passedMethodNames);
    }

    public Set<String> getFailedMethodNames()
    {
        return ImmutableSet.copyOf(failedMethodNames);
    }

    public Set<String> getIgnoredMethodNames()
    {
        return ImmutableSet.copyOf(ignoredMethodNames);
    }
}

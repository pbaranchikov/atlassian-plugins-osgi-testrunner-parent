package com.atlassian.plugins.osgi.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.plugins.osgi.test.util.Pair;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

/**
 * @since version
 */
public class OsgiTestClassLoader
{
    private static final String SERVICE_PROP_KEY = "inProductTest";
    private final BundleContext ctx;

    public OsgiTestClassLoader(final BundleContext ctx)
    {
        this.ctx = ctx;
    }

    public Pair<String, Object> findTestInstance(String testClassname)
    {
        Object rawTest = null;
        String bundleSymbolicName = "unknown";
        ServiceReference ref = ctx.getServiceReference(testClassname);
        if(null != ref)
        {
            rawTest = ctx.getService(ref);
            bundleSymbolicName = ref.getBundle().getSymbolicName();
        }

        return Pair.pair(bundleSymbolicName, rawTest);
    }

    public Map<Bundle, List<Object>> findAllTests()
    {
        HashMap<Bundle, List<Object>> testMap = new HashMap<Bundle, List<Object>>();

        StringBuilder sb = new StringBuilder("(");
        sb.append(SERVICE_PROP_KEY).append("=true)");

        try
        {
            ServiceReference[] refs = ctx.getAllServiceReferences(null, sb.toString());
            if (null != refs)
            {
                for (ServiceReference ref : refs)
                {
                    Bundle bundle = ref.getBundle();
                    if (!testMap.containsKey(bundle))
                    {
                        testMap.put(bundle, new ArrayList<Object>());
                    }

                    testMap.get(bundle).add(ctx.getService(ref));
                }
            }
        }
        catch (InvalidSyntaxException e)
        {
            //ignore
        }

        return testMap;
    }

    public Set<Bundle> findAllBundles()
    {
        Set<Bundle> bundles = new HashSet<Bundle>();
        StringBuilder sb = new StringBuilder("(");
        sb.append(SERVICE_PROP_KEY).append("=true)");

        try
        {
            ServiceReference[] refs = ctx.getAllServiceReferences(null, sb.toString());
            if (null != refs)
            {
                for (ServiceReference ref : refs)
                {
                    bundles.add(ref.getBundle());
                }
            }
        }
        catch (InvalidSyntaxException e)
        {
            //ignore
        }
        
        return bundles;
    }

    public List<Object> findAllTestsForBundle(String bundleSymbolicName)
    {
        List<Object> tests = new ArrayList<Object>();

        StringBuilder sb = new StringBuilder("(&(");
        sb.append(Constants.BUNDLE_SYMBOLICNAME)
          .append("=")
          .append(bundleSymbolicName)
          .append(")(")
          .append(SERVICE_PROP_KEY)
          .append("=true))");

        try
        {
            ServiceReference[] refs = ctx.getAllServiceReferences(null, sb.toString());
            if (null != refs)
            {
                for (ServiceReference ref : refs)
                {
                    tests.add(ctx.getService(ref));
                }
            }
        }
        catch (InvalidSyntaxException e)
        {
            //ignore
        }

        return tests;
    }
}

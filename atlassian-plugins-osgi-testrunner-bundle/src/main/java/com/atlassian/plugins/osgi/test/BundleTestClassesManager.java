package com.atlassian.plugins.osgi.test;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.atlassian.plugins.osgi.test.asm.BundleTestScanner;
import com.atlassian.plugins.osgi.test.util.Pair;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import org.osgi.framework.Bundle;

/**
 * @since version
 */
public class BundleTestClassesManager
{
    private static BundleTestClassesManager INSTANCE;
    private ConcurrentHashMap<String, Set<Class<?>>> unitTestClasses;
    private ConcurrentHashMap<String, Set<Class<?>>> itTestClasses;
    private CopyOnWriteArrayList<Bundle> bundles;
    
    private BundleTestClassesManager()
    {
        this.unitTestClasses = new ConcurrentHashMap<String, Set<Class<?>>>();
        this.itTestClasses = new ConcurrentHashMap<String, Set<Class<?>>>();
        this.bundles = new CopyOnWriteArrayList<Bundle>();
    }
    
    public static BundleTestClassesManager instance()
    {
        if(null == INSTANCE)
        {
            INSTANCE = new BundleTestClassesManager();
        }
        
        return INSTANCE;
    }
    
    public void registerBundle(Bundle bundle)
    {
        if(bundle.getSymbolicName().endsWith("-tests"))
        {
            BundleTestScanner scanner = new BundleTestScanner();
            Pair<Set<Class<?>>,Set<Class<?>>> unitAndItTests = scanner.scan(bundle);

            if(!unitAndItTests.first().isEmpty())
            {
                unitTestClasses.putIfAbsent(bundle.getSymbolicName(),unitAndItTests.first());
            }
            
            if(!unitAndItTests.second().isEmpty())
            {
                itTestClasses.putIfAbsent(bundle.getSymbolicName(),unitAndItTests.second());
            }

            if(unitAndItTests.first().size() > 0 || unitAndItTests.second().size() > 0)
            {
                bundles.addIfAbsent(bundle);
            }
            
        }
        
    }
    
    public void unRegisterBundle(Bundle bundle)
    {
        if(bundle.getSymbolicName().endsWith("-tests"))
        {
            if(unitTestClasses.containsKey(bundle.getSymbolicName()))
            {
                unitTestClasses.remove(bundle.getSymbolicName());
            }
            
            if(itTestClasses.containsKey(bundle.getSymbolicName()))
            {
                itTestClasses.remove(bundle.getSymbolicName());
            }
            
            bundles.remove(bundle);
        }
    }
    
    public Set<Class<?>> getUnitTests(String bundleSymbolicName)
    {
        if(unitTestClasses.containsKey(bundleSymbolicName))
        {
            return ImmutableSet.copyOf(unitTestClasses.get(bundleSymbolicName));
        }
        
        return Collections.emptySet();
    }

    public Set<Class<?>> getIntegrationTests(String bundleSymbolicName)
    {
        if(itTestClasses.containsKey(bundleSymbolicName))
        {
            return ImmutableSet.copyOf(itTestClasses.get(bundleSymbolicName));
        }

        return Collections.emptySet();
    }
    
    public Class<?> findTestClassInBundle(String bundleSymbolicName, final String classname)
    {
        Class<?> testClass = findTestClassInBundle(bundleSymbolicName,classname,unitTestClasses);
        
        if(null == testClass)
        {
            testClass = findTestClassInBundle(bundleSymbolicName,classname,itTestClasses);
        }
        
        return testClass;
    }

    public Pair<String,Class<?>> findTestClass(final String classname)
    {
        Pair<String,Class<?>> testClass = findTestClass(classname,unitTestClasses);

        if(null == testClass)
        {
            testClass = findTestClass(classname,itTestClasses);
        }

        return testClass;
    }

    public boolean hasMoreThanOneBundle()
    {
        Set<String> bundleKeys = new HashSet<String>(unitTestClasses.keySet());
        bundleKeys.addAll(itTestClasses.keySet());
        
        return (bundleKeys.size() > 1);
    }

    public boolean hasBundle(String bundleSymbolicName)
    {
        Set<String> bundleKeys = new HashSet<String>(unitTestClasses.keySet());
        bundleKeys.addAll(itTestClasses.keySet());

        return bundleKeys.contains(bundleSymbolicName);
    }

    public List<Bundle> getAllBundles()
    {
        return ImmutableList.copyOf(bundles);
    }

    public Set<String> getAllBundleKeys()
    {
        Set<String> bundleKeys = new HashSet<String>(unitTestClasses.keySet());
        bundleKeys.addAll(itTestClasses.keySet());
        
        return ImmutableSet.copyOf(bundleKeys);
    }
    
    
    private Class<?> findTestClassInBundle(String bundleSymbolicName, final String classname,Map<String,Set<Class<?>>> classMap)
    {
        Class<?> testClass = null;
        Set<Class<?>> classes = classMap.get(bundleSymbolicName);
        if(null != classes)
        {
            for(Class<?> classToCheck : classes)
            {
                if(classname.equals(classToCheck.getName()))
                {
                    testClass = classToCheck;
                    break;
                }
            }
        }
        
        return testClass;
    }

    private Pair<String,Class<?>> findTestClass(final String classname,Map<String,Set<Class<?>>> classMap)
    {
        Class<?> testClass = null;
        String bundleSymbolicName = null;

        for(Map.Entry<String,Set<Class<?>>> unitEntry : classMap.entrySet())
        {
            bundleSymbolicName = unitEntry.getKey();
            
            try
            {
                testClass = Iterables.find(unitEntry.getValue(),new Predicate<Class<?>>() {
                    @Override
                    public boolean apply(Class<?> input)
                    {
                        return classname.equals(input.getName());
                    }
                });
            }
            catch (NoSuchElementException e)
            {
                //ignore
            }

            if(null != testClass)
            {
                break;
            }
        }

        if(null == testClass)
        {
            return null;
        }

        return Pair.<String,Class<?>> pair(bundleSymbolicName,testClass);
    }
}

package com.atlassian.plugins.osgi.test.rest;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.plugin.util.ClassLoaderStack;
import com.atlassian.plugins.osgi.test.BundleTestClassesManager;
import com.atlassian.plugins.osgi.test.JUnitOSGiTestExecutor;
import com.atlassian.plugins.osgi.test.OsgiTestClassLoader;
import com.atlassian.plugins.osgi.test.util.Pair;
import com.atlassian.plugins.osgi.test.util.TestClassUtils;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.upm.api.util.Option;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.internal.runners.JUnit38ClassRunner;
import org.junit.runner.*;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;

@Path("/")
public class TestRunnerResource
{
    private OsgiTestClassLoader testLoader;


    public TestRunnerResource(OsgiTestClassLoader testLoader)
    {
        this.testLoader = testLoader;
    }

    @GET
    @Path("runtest/{testClassName}")
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    public Response runTest(@PathParam("testClassName") String testClassName)
    {
        String json;

        Gson gson = getGson();

        TestResultDetailRepresentation result = executeTest(testClassName).getResult();
        json = gson.toJson(result);

        return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("runtestfromconsole/{testClassName}")
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    public Response runTestFromConsole(@PathParam("testClassName") String testClassName)
    {
        BundleResultAndTestClass resultAndClass = executeTest(testClassName);
        String bundleSymbolicName = resultAndClass.getBundleSymbolicName();

        TestResultDetailRepresentation testResultDetail = resultAndClass.getResult();

        BundleResultRepresentation bundleResult = new BundleResultRepresentation(bundleSymbolicName, testResultDetail);

        Gson gson = getGson();
        String json = gson.toJson(bundleResult);

        return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("testlist/{bundleSymbolicName}")
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    public Response testList(@PathParam("bundleSymbolicName") String bundleSymbolicName)
    {
        Response rsp;

        List<Object> osgiTests = testLoader.findAllTestsForBundle(bundleSymbolicName);

        BundleTestListRepresentation bundleList = new BundleTestListRepresentation(bundleSymbolicName);
        List<TestDetailRepresentation> wiredTestHolders = bundleList.getWiredTests();
        List<TestDetailRepresentation> unitTestHolders = bundleList.getUnitTests();
        List<TestDetailRepresentation> itTestHolders = bundleList.getITTests();

        TestDetailComparator comparator = new TestDetailComparator();

        for (Object osgiTest : osgiTests)
        {
            loadTestDetails(osgiTest.getClass(), wiredTestHolders);
        }


        Set<Class<?>> unitClasses = BundleTestClassesManager.instance().getUnitTests(bundleSymbolicName);

        for (Class<?> unitClass : unitClasses)
        {
            loadTestDetails(unitClass, unitTestHolders);
        }

        Set<Class<?>> itClasses = BundleTestClassesManager.instance().getIntegrationTests(bundleSymbolicName);

        for (Class<?> itClass : itClasses)
        {
            loadTestDetails(itClass, itTestHolders);
        }

        Collections.sort(wiredTestHolders, comparator);
        Collections.sort(unitTestHolders, comparator);
        Collections.sort(itTestHolders, comparator);

        Gson gson = getGson();
        String json = gson.toJson(bundleList);

        rsp = Response.ok(json, MediaType.APPLICATION_JSON).build();


        return rsp;
    }

    protected Gson getGson()
    {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Annotation.class, new AnnotationTypeAdapter());
        gsonBuilder.registerTypeAdapter(Option.class, new UpmOptionAdapter());

        return gsonBuilder.create();
    }
    
    protected BundleResultAndTestClass executeTest(String testClassName)
    {
        Class<?> testClass = null;
        String bundleSymbolicName = "";
        Result testResult = new Result();
        TestResultDetailRepresentation resultDetail = new TestResultDetailRepresentation(testClassName, testResult);

        try
        {
            Pair<Runner, Pair<String, Class>> runnerAndTest = getRunnerAndTest(testClassName);
            Runner runner = runnerAndTest.first();
            bundleSymbolicName = runnerAndTest.second().first();
            testClass = runnerAndTest.second().second();

            JUnitCore c = new JUnitCore();
            try {
                ClassLoaderStack.push(testClass.getClassLoader());
                testResult = c.run(Request.runner(runner));
            } finally {
                ClassLoaderStack.pop();
            }

            //add failures first
            //make sure we have stack traces
            for (Failure f : testResult.getFailures())
            {
                f.getException().getStackTrace();
                f.getException().printStackTrace();
                resultDetail.getFailedMethods().put(f.getDescription().getMethodName(), f);
            }

            //if we didn't run the test because we couldn't find the class, we need to mark it as a failure
            if (null == testClass)
            {
                Description desc = Description.createSuiteDescription(testClassName, new Annotation[0]);
                Failure classNotFoundFail = new Failure(desc, new ClassNotFoundException("Unable to find class for test [" + testClassName + "]"));

                resultDetail.getFailedMethods().put(testClassName, classNotFoundFail);
            }

            //now add the passed and ignored methods
            if (JUnitOSGiTestExecutor.class.isAssignableFrom(runner.getClass()))
            {
                JUnitOSGiTestExecutor osgiRunner = (JUnitOSGiTestExecutor) runner;

                resultDetail.getIgnoredMethods().addAll(osgiRunner.getIgnoredMethodNames());
                resultDetail.getPassedMethods().addAll(osgiRunner.getPassedMethodNames());
            }
            else
            {
                //add ignored methods
                resultDetail.getIgnoredMethods().addAll(TestClassUtils.getIgnoredMethodNames(testClass));

                //add passed methods
                List<String> ranMethods = TestClassUtils.getNonIgnoredMethodNames(testClass);
                Set<String> failedMethodNames = resultDetail.getFailedMethods().keySet();

                for (String methodName : ranMethods)
                {
                    if (!failedMethodNames.contains(methodName))
                    {
                        resultDetail.getPassedMethods().add(methodName);
                    }
                }
            }
        }
        catch (InitializationError ie)
        {
            testResult = new Result();
            resultDetail = new TestResultDetailRepresentation(testClassName, testResult);
            RunListener listener = testResult.createListener();
            try
            {
                ie.getStackTrace();
                for (Throwable t : ie.getCauses())
                {
                    t.getStackTrace();
                    Failure f = new Failure(Description.createSuiteDescription(testClassName), t);
                    listener.testFailure(f);
                    resultDetail.getFailedMethods().put("initialization", f);
                }

            }
            catch (Exception e)
            {
                //do nothing
            }
        }
        catch (Throwable t)
        {
            testResult = new Result();
            resultDetail = new TestResultDetailRepresentation(testClassName, testResult);
            RunListener listener = testResult.createListener();
            try
            {
                t.getStackTrace();
                t.getCause().getStackTrace();
                Failure f = new Failure(Description.createSuiteDescription(testClassName), t.getCause());
                listener.testFailure(f);
                resultDetail.getFailedMethods().put("exception", f);
            }
            catch (Exception e)
            {
            }
        }

        return new BundleResultAndTestClass(bundleSymbolicName, resultDetail, testClass);
    }

    private Pair<Runner, Pair<String, Class>> getRunnerAndTest(String testClassName) throws InitializationError
    {
        Runner runner = null;
        Class testClass;

        Pair<String, Object> osgiTestPair = testLoader.findTestInstance(testClassName);
        String bundleSymbolicName = osgiTestPair.first();
        Object osgiTest = osgiTestPair.second();

        if (null != osgiTest)
        {
            testClass = osgiTest.getClass();
            runner = new JUnitOSGiTestExecutor(testClass, osgiTest);
        }
        else
        {
            Pair<String, Class<?>> testPair = BundleTestClassesManager.instance().findTestClass(testClassName);
            testClass = testPair != null ? testPair.second() : null;
            bundleSymbolicName = testPair != null ? testPair.first() : null;
            if (null != testClass)
            {
                switch (TestClassUtils.getTestType(testClass))
                {
                    case IT:
                        if (junit.framework.TestCase.class.isAssignableFrom(testClass))
                        {
                            runner = new JUnit38ClassRunner(testClass);
                        }
                        else
                        {
                            runner = new BlockJUnit4ClassRunner(testClass);
                        }
                        break;
                    case Unit:
                        if (junit.framework.TestCase.class.isAssignableFrom(testClass))
                        {
                            runner = new JUnit38ClassRunner(testClass);
                        }
                        else
                        {
                            runner = new BlockJUnit4ClassRunner(testClass);
                        }
                        break;
                }
            }
        }

        if (null == runner)
        {
            throw new InitializationError("unable to find runner for class: " + testClassName);
        }

        return Pair.pair(runner, Pair.pair(bundleSymbolicName, testClass));
    }

    private void loadTestDetails(Class<?> testClass, List<TestDetailRepresentation> testDetails)
    {
        try
        {
            TestDetailRepresentation detail = new TestDetailRepresentation(testClass.getName());
            detail.setAllTestMethods(TestClassUtils.getAllTestMethodNames(testClass));

            testDetails.add(detail);
        }
        catch (NoClassDefFoundError e)
        {
            // the test class references classes that are not visible in the current classloader - discard test
            // class; it is not runnable from the osgi-testrunner
        }
    }

    public class BundleResultAndTestClass
    {
        private final String bundleSymbolicName;
        private final TestResultDetailRepresentation result;
        private final Class<?> testClass;

        public BundleResultAndTestClass(String bundleSymbolicName, TestResultDetailRepresentation result, Class<?> testClass)
        {
            this.result = result;
            this.testClass = testClass;
            this.bundleSymbolicName = bundleSymbolicName;
        }

        public TestResultDetailRepresentation getResult()
        {
            return result;
        }

        public Class<?> getTestClass()
        {
            return testClass;
        }

        public String getBundleSymbolicName()
        {
            return bundleSymbolicName;
        }
    }

    private class TestDetailComparator implements Comparator<TestDetailRepresentation>
    {
        @Override
        public int compare(TestDetailRepresentation o1, TestDetailRepresentation o2)
        {
            return o1.getClassname().compareTo(o2.getClassname());
        }
    }

}
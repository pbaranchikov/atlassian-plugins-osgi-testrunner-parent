AJS.$(function ($) {
    var localTestConsole = function() 
    {
        this.storageKey = "ittestconsole-classes";

        this.storage = new $.store();
        this.testQueue = new Array();
        this.testsAreRunning = false;
        this.bundleSymbolicName = $("#test-console-container").attr("bundle-key");
        
        this.spinnerOpts = {
            lines: 13, // The number of lines to draw
            length: 5, // The length of each line
            width: 2, // The line thickness
            radius: 4, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            color: '#000', // #rgb or #rrggbb
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinjs', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: 0, // Top position relative to parent in px
            left: 4 // Left position relative to parent in px
        };

        this.loadTests = function()
        {
            var me = this;
            $.ajax({
                url: AJS.contextPath() + "/rest/atlassiantestrunner/1.0/testlist/" + me.bundleSymbolicName,
                type: 'get',
                cache: false,
                success:function (testListJson) {
                    var wiredTests = testListJson.testMap.Wired;
                    var itTests = testListJson.testMap.IT;
                    var unitTests = testListJson.testMap.Unit;
                    
                    var totalTests = wiredTests.length + itTests.length + unitTests.length;
                    
                    if(wiredTests.length > 0)
                    {
                        $("#test-console-container").append(TESTCONSOLE.Templates.testTable({testType:{title:AJS.I18n.getText('it.test.console.wired-title'),type:AJS.I18n.getText('it.test.console.wired')}}));
                        
                        for (var i = 0, len = wiredTests.length; i < len; i++)
                        {
                            var test = wiredTests[i];
                            $("#wired-test-table-body").append(TESTCONSOLE.Templates.testEntry({test:test}));
                        }
                        
                        me.setupTestTypeBinds(AJS.I18n.getText('it.test.console.wired'),$('#wired-test-table'));
                        $('#wired-run-all-button').text(AJS.I18n.getText('it.test.console.button.run-all-type',wiredTests.length,AJS.I18n.getText('it.test.console.wired')));
                    }

                    if(itTests.length > 0)
                    {
                        $("#test-console-container").append(TESTCONSOLE.Templates.testTable({testType:{title:AJS.I18n.getText('it.test.console.integration-title'),type:AJS.I18n.getText('it.test.console.integration')}}));

                        for (var i = 0, len = itTests.length; i < len; i++)
                        {
                            var test = itTests[i];
                            $("#integration-test-table-body").append(TESTCONSOLE.Templates.testEntry({test:test}));
                        }

                        me.setupTestTypeBinds(AJS.I18n.getText('it.test.console.integration'),$('#integration-test-table'));
                        $('#integration-run-all-button').text(AJS.I18n.getText('it.test.console.button.run-all-type',itTests.length,AJS.I18n.getText('it.test.console.integration')));
                    }

                    if(unitTests.length > 0)
                    {
                        $("#test-console-container").append(TESTCONSOLE.Templates.testTable({testType:{title:AJS.I18n.getText('it.test.console.unit-title'),type:AJS.I18n.getText('it.test.console.unit')}}));

                        for (var i = 0, len = unitTests.length; i < len; i++)
                        {
                            var test = unitTests[i];
                            $("#unit-test-table-body").append(TESTCONSOLE.Templates.testEntry({test:test}));
                        }

                        me.setupTestTypeBinds(AJS.I18n.getText('it.test.console.unit'),$('#unit-test-table'));
                        $('#unit-run-all-button').text(AJS.I18n.getText('it.test.console.button.run-all-type',unitTests.length,AJS.I18n.getText('it.test.console.unit')));
                    }

                    
                    if(totalTests > 0)
                    {
                        me.enableRunAll();
                        $('#run-all-button').text(AJS.I18n.getText('it.test.console.button.run-all',totalTests));
                    }
                    
                    me.setupBinds();
                    
                    var testsToRun = me.storage.get(me.storageKey);
                    if($.isArray(testsToRun))
                    {
                        me.storage.del(me.storageKey);
                        
                        me.queueAndTrigger(testsToRun);
                    }
                }
            });
        };

        this.queueAndTrigger = function(testClasses)
        {
            var me = this;
            if(testClasses.length > 0)
            {
                $.each(testClasses, function(index,value){
                    me.testQueue.push(value.classname);
                    var testDiv = me.dotsToDashes(value.classname);
                    
                    $('#' + testDiv + ' .passedcount').text("");
                    $('#' + testDiv + ' .ignoredcount').text("");
                    $('#' + testDiv + ' .failedcount').text("");
                    $('#' + testDiv + ' .durationsec').text("");
                    $("#" + testDiv + '-result').remove();
                    
                    var runButtonId = testDiv + "-runButton";
                    
                    var runButton = $('#' + runButtonId);

                    runButton.html(TESTCONSOLE.Templates.queuedButton());
                    
                });
                
                if(!me.testsAreRunning)
                {
                    me.disableRunAll();
                    me.triggerJUnit(me.testQueue.shift());
                }
            }
        };
        
        this.unQueueTest = function(classname)
        {
            this.testQueue = $.grep(this.testQueue, function(value) {
                return value != classname;
            });

            var runButtonId = this.dotsToDashes(classname) + "-runButton";
            var runButton = $('#' + runButtonId);
            runButton.html(TESTCONSOLE.Templates.playButton());
        }
        
        
        this.dotsToDashes = function(name)
        {
            return name.replace(/\./g,'-');
        };

        this.dashesToDots = function(name)
        {
            return name.replace(/-/g,'.');
        };

        this.setupBinds = function()
        {
            var me = this;
            $('.playTestButton').live('click',function(evt){
                evt.preventDefault();
                me.triggerFastdev([{classname:me.dashesToDots($(this).parent().parent().attr('id'))}]);
                
            });

            $('.stopTestButton').live('click',function(evt){

                evt.preventDefault();
                var parent = $(this).parent();
                me.unQueueTest(me.dashesToDots($(this).parent().parent().attr('id')));

            });
            
            $('.testClassName').live('click',function(evt){
                evt.preventDefault();
                var resultRow = $('#' + $(this).parent().attr('id') + "-result");
                resultRow.toggle();
                if(resultRow.is(":visible"))
                {
                    $(this).parent().removeClass('testClassRowCollapsed');
                    $(this).parent().addClass('testClassRowExpanded');
                }
                else
                {
                    $(this).parent().removeClass('testClassRowExpanded');
                    $(this).parent().addClass('testClassRowCollapsed');
                }
                
            });
        };

        this.setupTestTypeBinds = function(testType,testTable)
        {
            var me = this;
            $('#' + testType + '-run-all-button').click(function(evt){
                evt.preventDefault();
                if(!$(this).parent().is('.disabled'))
                {
                    var testsToRun = new Array();
                    $('.testClassRow',testTable).each(function(){
                        testsToRun.push({classname:me.dashesToDots($(this).attr('id'))});
                    });

                    if(testsToRun.length > 0)
                    {
                        me.triggerFastdev(testsToRun);
                    }
                }

            });

            $('#' + testType + '-run-failed-button').click(function(evt){
                evt.preventDefault();
                if(!$(this).parent().is('.disabled'))
                {
                    var testsToRun = new Array();
                    $('.testClassRow>.failedcount',testTable).each(function(){
                        var fcount = me.getCountForElement($(this));
                        if(fcount > 0)
                        {
                            testsToRun.push({classname:me.dashesToDots($(this).parent().attr('id'))});
                        }
                    });

                    if(testsToRun.length > 0)
                    {
                        me.triggerFastdev(testsToRun);
                    }
                }
            });
        };

        this.triggerFastdev = function(testClasses)
        {
            var me = this;
            $.ajax({
                type: 'POST',
                url: AJS.contextPath() + '/rest/fastdev/1.0/reload/withtests',
                dataType: 'json',
                success: function(d,s,x){
                    if (x.status === 204) {

                        $('.dt-msg-fastdev').addClass('dt-msg-show').animate({
                            bottom: '+=85',
                            opacity: 1
                        },'2000','swing',function(){
                            var self = $(this);
                            setTimeout(function(){
                                self.animate({
                                    bottom: '-=85',
                                    opacity: 0
                                });
                                self.removeClass('dt-msg-show');
                            }, 1000);
                        });
                        
                        if(null != testClasses && $.isArray(testClasses) && testClasses.length > 0)
                        {
                            me.queueAndTrigger(testClasses);
                        }

                    } else {
                        if(null != testClasses && $.isArray(testClasses) && testClasses.length > 0)
                        {
                            me.storage.set(me.storageKey,testClasses);
                        }
                        var myUrl = $.param.querystring(window.location.href, 'bundle=' + me.bundleSymbolicName);
                        window.location = AJS.contextPath() + '/plugins/servlet/fastdev?origUrl=' + encodeURIComponent(myUrl);
                    }
                }
            });
        };

        this.triggerJUnit = function(testClass)
        {
            var me=this;
            me.testsAreRunning = true;
            var testDiv = this.dotsToDashes(testClass);
            
            this.updateTotalCounts();

            var runButtonId = testDiv + "-runButton";

            var runButton = $('#' + runButtonId);

            runButton.html(TESTCONSOLE.Templates.running());
            new Spinner(me.spinnerOpts).spin(runButton.children('.testSpinner').get(0));
            
            $.ajax({
                type: 'GET',
                url: AJS.contextPath() + '/rest/atlassiantestrunner/1.0/runtestfromconsole/' + testClass,
                dataType: 'json',
                success: function(testBundleResult){
                    $("#" + testDiv).replaceWith(TESTCONSOLE.Templates.testEntry({test:testBundleResult.testResultDetail}));
                    $("#" + testDiv + '-result .fail-more-link').text(AJS.I18n.getText("it.test.console.failed.more"));

                    $("#" + testDiv + '-result .fail-more-link').click(function (evt){
                        evt.preventDefault();
                        var exceptionContainer = $(this).parent().next();
                        exceptionContainer.toggle();
                        if(exceptionContainer.is(":visible"))
                        {
                            $(this).text(AJS.I18n.getText("it.test.console.failed.less"));
                        }
                        else
                        {
                            $(this).text(AJS.I18n.getText("it.test.console.failed.more"));
                        }
                    });
                    me.updateTotalCounts();
                    
                    if(me.testQueue.length > 0)
                    {
                        me.triggerJUnit(me.testQueue.shift());
                    }
                    else
                    {
                        me.testsAreRunning = false;
                        me.enableRunAll();
                    }
                },
                error: function(request,status,error){
                    var test = {classname:testClass,divId:testDiv,status:status,error:error};
                    $("#" + testDiv).replaceWith(TESTCONSOLE.Templates.testEntryServerError({test:test}));
                    
                    me.updateTotalCounts();

                    if(me.testQueue.length > 0)
                    {
                        me.triggerJUnit(me.testQueue.shift());
                    }
                    else
                    {
                        me.testsAreRunning = false;
                        me.enableRunAll();
                    }
                }
            });
        };
        
        this.updateTotalCounts = function()
        {
            var me = this;
            var totalFailed = 0;
            
            $('.test-table').each(function(){
                var passed = me.getPassedCount($(this));
                var ignored = me.getIgnoredCount($(this));
                var failed = me.getFailedCount($(this));
               
                $('.numpassed>span',$(this)).text(AJS.I18n.getText("it.test.console.lozenge.passed",passed));
                $('.numignored>span',$(this)).text(AJS.I18n.getText("it.test.console.lozenge.ignored",ignored));
                $('.numfailed>span',$(this)).text(AJS.I18n.getText("it.test.console.lozenge.failed",failed));
                
                var myType = $(this).attr('id').split("-")[0];
                var numFailedTestClasses = me.getFailedTestCount($(this));
                $('#' + myType + '-run-failed-button').text(AJS.I18n.getText('it.test.console.button.run-failed-type',numFailedTestClasses,myType));
                
            });

            $('.testClassRow>.failedcount').each(function(){
                var fcount = me.getCountForElement($(this));
                if(fcount > 0)
                {
                    totalFailed = totalFailed + 1;
                }
            });
            
            
            $('#run-failed-button').text(AJS.I18n.getText('it.test.console.button.run-failed',totalFailed));
        };
        
        this.getPassedCount = function(testTable)
        {
            return this.getCount(testTable,".passedcount");
        };

        this.getIgnoredCount = function(testTable)
        {
            return this.getCount(testTable,".ignoredcount");
        };

        this.getFailedCount = function(testTable)
        {
            return this.getCount(testTable,".failedcount");
        };

        this.getFailedTestCount = function(testTable)
        {
            return this.getTestCount(testTable,".failedcount");
        };

        this.getCount = function(testTable,type)
        {
            var count = 0;
            var me = this;
            $(type,testTable).each(function(){
                if(me.isNumber($.trim($(this).text())))
                {
                    count = count + parseInt($.trim($(this).text()));
                }
            });

            return count;
        };

        this.getTestCount = function(testTable,type)
        {
            var count = 0;
            var me = this;
            $(type,testTable).each(function(){
                if(me.isNumber($.trim($(this).text())))
                {
                    count = count + 1;
                }
            });

            return count;
        };

        this.isNumber = function(n)
        {
            return !isNaN(parseFloat(n)) && isFinite(n);
        };
        
        this.enableRunAll = function()
        {
            var me = this;
            $('#run-all-button').parent().removeClass('disabled');
            $('#scan-for-tests-button').parent().removeClass('disabled');
            
            var failed = this.getFailedCount();
            if(failed > 0)
            {
                $('#run-failed-button').parent().removeClass('disabled');
            }
            else
            {
                $('#run-failed-button').parent().addClass('disabled');
            }

            $('.type-run-all-button').each(function (){
                $(this).parent().removeClass('disabled');
            });
            
            
            $('.test-table').each(function(){
                var failedTypeCount = me.getFailedCount($(this));
                var testType = $(this).attr('id').split('-')[0];
                
                if(failedTypeCount > 0)
                {
                    $('#' + testType + '-run-failed-button').parent().removeClass('disabled');
                }
                else
                {
                    $('#' + testType + '-run-failed-button').parent().addClass('disabled');
                }
            });
        };

        this.disableRunAll = function()
        {
            $('#scan-for-tests-button').parent().addClass('disabled');
            $('#run-all-button').parent().addClass('disabled');
            $('#run-failed-button').parent().addClass('disabled');

            $('.type-run-all-button').each(function (){
                $(this).parent().addClass('disabled');
            });

            $('.type-run-failed-button').each(function (){
                $(this).parent().addClass('disabled');
            });

        };
        

        this.getCountForElement = function(elem)
        {
            var count = 0;
            if(this.isNumber($.trim(elem.text())))
            {
                count = count + parseInt($.trim(elem.text()));
            }
            return count;
        }
        
        this.init = function()
        {
            var me = this;
            $('#scan-for-tests-button').click(function(evt){
                evt.preventDefault();
                me.triggerFastdev(null);
            });

            $('#run-all-button').click(function(evt){
                evt.preventDefault();
                if(!$(this).parent().is('.disabled'))
                {
                    var testsToRun = new Array();
                    $('.testClassRow').each(function(){
                        testsToRun.push({classname:me.dashesToDots($(this).attr('id'))});
                    });
                    
                    if(testsToRun.length > 0)
                    {
                        me.triggerFastdev(testsToRun);
                    }
                }
                
            });

            $('#run-failed-button').click(function(evt){
                evt.preventDefault();
                if(!$(this).parent().is('.disabled'))
                {
                    var testsToRun = new Array();
                    $('.testClassRow>.failedcount').each(function(){
                        var fcount = me.getCountForElement($(this));
                        if(fcount > 0)
                        {
                            testsToRun.push({classname:me.dashesToDots($(this).parent().attr('id'))});
                        }
                    });

                    if(testsToRun.length > 0)
                    {
                        me.triggerFastdev(testsToRun);
                    }
                }
            });
            
            this.loadTests();
        };
        
    };
    
    window.itTestConsole = new localTestConsole();

    itTestConsole.init();
    
});
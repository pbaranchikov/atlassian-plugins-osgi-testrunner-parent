AJS.$(function ($) {
    var localTestBundles = function()
    {
        this.init = function()
        {
            var me = this;
            $('.bundleRow').click(function(){
                window.location.href = AJS.contextPath() + "/plugins/servlet/it-test-console?bundle=" + me.underscoreToDots($(this).attr('id'));
            });
        };

        this.underscoreToDots = function(name)
        {
            return name.replace(/\_/g,'.');
        };
    };

    window.itTestBundles = new localTestBundles();

    itTestBundles.init();
    
});
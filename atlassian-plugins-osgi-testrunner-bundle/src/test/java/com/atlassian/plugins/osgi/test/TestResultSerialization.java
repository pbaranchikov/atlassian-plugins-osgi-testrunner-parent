package com.atlassian.plugins.osgi.test;

import java.lang.annotation.Annotation;

import com.atlassian.plugins.osgi.test.rest.AnnotationTypeAdapter;
import com.atlassian.plugins.osgi.test.rest.UpmOptionAdapter;
import com.atlassian.upm.api.util.Option;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.RunWith;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.junit.runners.BlockJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;

/**
 * @since version
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class TestResultSerialization
{

    @Test
    public void resultSerializesWithGson() throws Exception
    {
        Annotation[] annos = getClass().getAnnotations();
        Result result = new Result();
        RunListener listener = result.createListener();
        Description description = Description.createTestDescription(TestResultSerialization.class,TestResultSerialization.class.getName(),annos);
        listener.testRunStarted(description);
        ComparisonFailure exception = new ComparisonFailure("failed test","expected this","got that");
        exception.getStackTrace();
        
        Failure failure = new Failure(description,exception);

        listener.testFailure(failure);
        listener.testFinished(description);

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Annotation.class, new AnnotationTypeAdapter());
        gsonBuilder.registerTypeAdapter(Option.class, new UpmOptionAdapter());
        
        Gson gson = gsonBuilder.create();

        String json = gson.toJson(result);
        
        assertNotNull(json);
    }


    @Test
    public void resultDeserializesWithGson() throws Exception
    {

        Annotation[] annos = getClass().getAnnotations();
        Result result = new Result();
        RunListener listener = result.createListener();
        Description description = Description.createTestDescription(TestResultSerialization.class,TestResultSerialization.class.getName(),annos);
        listener.testRunStarted(description);
        ComparisonFailure exception = new ComparisonFailure("failed test","expected this","got that");
        exception.getStackTrace();

        Failure failure = new Failure(description,exception);
        listener.testFailure(failure);
        listener.testFinished(description);

        GsonBuilder gsonSerializeBuilder = new GsonBuilder();
        gsonSerializeBuilder.registerTypeAdapter(Annotation.class, new AnnotationTypeAdapter());
        gsonSerializeBuilder.registerTypeAdapter(Option.class, new UpmOptionAdapter());

        Gson gsonSerializer = gsonSerializeBuilder.create();

        String json = gsonSerializer.toJson(result);

        GsonBuilder gsonDeSerializeBuilder = new GsonBuilder();
        gsonDeSerializeBuilder.registerTypeAdapter(Annotation.class, new AnnotationTypeAdapter());
        gsonDeSerializeBuilder.registerTypeAdapter(Option.class, new UpmOptionAdapter());

        Gson gsonDeSerializer = gsonDeSerializeBuilder.create();

        Result osgiResult = gsonDeSerializer.fromJson(json,Result.class);

        assertNotNull(osgiResult);
    }
    
    
}
